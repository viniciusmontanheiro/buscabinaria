//
//  Dados.h
//  buscaBinaria
//
//  Created by Vinícius Montanheiro on 10/03/15.
//  Copyright (c) 2015 Vinícius Montanheiro. All rights reserved.
//

#include "iostream"
using namespace std;
#include<string.h>

#ifndef __buscaBinaria__Dados__
#define __buscaBinaria__Dados__
class Dados{
private:
    int id;
    string nome;
    string endereco;
    char sexo;
    string telefone;
    
public:
    Dados();
    void setId(int id);
    int getId();
    
    void setNome(string nome);
    string getNome();
    
    void setEndereco(string endereco);
    string getEndereco();
    
    void setSexo(char sexo);
    char getSexo();
    
    void setTelefone(string telefone);
    string getTelefone();
};

#endif /* defined(__buscaBinaria__Dados__) */



