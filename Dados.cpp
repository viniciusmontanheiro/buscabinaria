//
//  Dados.cpp
//  buscaBinaria
//
//  Created by Vinícius Montanheiro on 10/03/15.
//  Copyright (c) 2015 Vinícius Montanheiro. All rights reserved.
//

#include "Dados.h"

Dados::Dados(){
    this->id = 0;
    this->nome = "";
    this->endereco = "";
    this->sexo = NULL;
    this->telefone = "";
};

void Dados::setId(int id){
    this->id = id;
}

int Dados::getId(){
    return this->id;
}

void Dados::setNome(string nome){
    this->nome = nome;
}

string Dados::getNome(){
    return this->nome;
}

void Dados::setEndereco(string endereco){
    this->endereco = endereco;
}

string Dados::getEndereco(){
    return this->endereco;
}

void Dados::setSexo(char sexo){
    this->sexo = sexo;
}

char Dados::getSexo(){
    return this->sexo;
}

void Dados::setTelefone(string telefone){
    this->telefone = telefone;
}

string Dados::getTelefone(){
    return this->telefone;
}
