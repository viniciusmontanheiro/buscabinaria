#include "Dados.h"

int buscaBinaria(Dados *vet, int valor, int min, int max){
    int meio = (min + max) / 2;
    
    if (valor == vet[meio].getId()){
        return meio;
    }
    else{
        if (valor < vet[meio].getId()){
            return buscaBinaria (vet, valor, min, meio-1);
        }
        else{
            return buscaBinaria (vet, valor, meio+1, max);
        }
    }
}

void mostrar(int i, Dados *vet){
    if(!vet[i].getId()){
        cout << "ID não encontrado!" ;
    }
    else{
        cout << "\n##########################################################" ;
        cout << "\n Posição no vetor: " << i << endl;
        cout << " ID: " << vet[i].getId() << endl;
        cout << " Nome: " << vet[i].getNome() << endl;
        cout << " Endereço: " << vet[i].getEndereco() << endl;
        cout << " Sexo: " << (vet[i].getSexo() == 'm' ? "masculino" : "feminino") << endl;
        cout << " Telefone: " << vet[i].getTelefone() << endl;
        cout << "##########################################################" << endl ;
    }
}

int main(){
    int t = 3;
    Dados obj,obj1,obj2;
    Dados *vet = new Dados[t];
    int posicao;
    
    obj.setId(10);
    obj.setNome("Vinícius Montanheiro");
    obj.setSexo('m');
    obj.setEndereco("Testando endereço");
    obj.setTelefone("64-99246422");
    
    obj1.setId(8);
    obj1.setNome("Fulana da Silva");
    obj1.setSexo('f');
    obj1.setEndereco("Testando 2 endereço");
    obj1.setTelefone("64-98774457");
    
    obj2.setId(45);
    obj2.setNome("Ciclano da Silva");
    obj2.setSexo('m');
    obj2.setEndereco("Testando 2 endereço");
    obj2.setTelefone("64-98774457");
    
    vet[0] = obj;
    vet[1] = obj1;
    vet[2] = obj2;
    
    cout << "Informe o id a ser buscado : ";
    cin >> posicao;
    
    posicao = buscaBinaria(vet, posicao, 0, t - 1);
    mostrar(posicao, vet);
}

